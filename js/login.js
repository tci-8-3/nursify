// Import the functions you need from the SDKs you need
import { initializeApp } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js';
import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js'
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";


const firebaseConfig = {
    apiKey: "AIzaSyBI6RyK4QIWVvGaVqdaVxlXJXr8GTX2xig",
    authDomain: "nursifyweb.firebaseapp.com",
    projectId: "nursifyweb",
    storageBucket: "nursifyweb.appspot.com",
    messagingSenderId: "450700024981",
    appId: "1:450700024981:web:ab5581390f43a8d7d50f9c"
};


// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const auth = getAuth(app);

const btnLogin = document.getElementById('btnLogin');

function login() {
    const email = document.getElementById('usuario').value;
    const passwd = document.getElementById('contra').value;

    signInWithEmailAndPassword(auth, email, passwd).then(() => {
            alert("Se inicio sesion correctamente");
            window.location.href = "/html/menu.html";
        })
        .catch(() => {
            alert("El correo o la contraseña son incorrectas");
        });
}

btnLogin.addEventListener('click', () => {
    login();
});