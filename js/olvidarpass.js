// Importar las funciones necesarias de los SDK que se requieren
import { initializeApp } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js';
import { getAuth, sendPasswordResetEmail } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js';

// Configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyBI6RyK4QIWVvGaVqdaVxlXJXr8GTX2xig",
    authDomain: "nursifyweb.firebaseapp.com",
    projectId: "nursifyweb",
    storageBucket: "nursifyweb.appspot.com",
    messagingSenderId: "450700024981",
    appId: "1:450700024981:web:ab5581390f43a8d7d50f9c"
};

// Inicializar Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

// Obtener referencia a los elementos del DOM
const btnOlvidarContra = document.getElementById('btnOlvidarContra');
const correoInput = document.getElementById('correo');

// Función para validar el correo electrónico
function validarCorreo(correo) {
    const regexCorreo = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regexCorreo.test(correo);
}

// Función para enviar el correo de restablecimiento
function enviarCorreoRestablecimiento() {
    const correo = correoInput.value;

    // Validar el correo electrónico
    if (!validarCorreo(correo)) {
        alert("Ingrese un correo electrónico válido.");
        return;
    }

    // Enviar el correo de restablecimiento
    sendPasswordResetEmail(auth, correo)
        .then(() => {
            // Correo enviado exitosamente
            alert("Se ha enviado un correo electrónico para restablecer tu contraseña.");
            // Redirigir al usuario al inicio de sesión
            window.location.href = "/index.html";
        })
        .catch((error) => {
            // Error al enviar el correo de restablecimiento
            alert("Hubo un error al enviar el correo de restablecimiento: " + error.message);
        });
}


// Agregar evento click al botón de olvidar contraseña
btnOlvidarContra.addEventListener('click', () => {
    enviarCorreoRestablecimiento();
});