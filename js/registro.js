// Importar las funciones necesarias de los SDK que se requieren
import { initializeApp } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js';
import { getAuth, createUserWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js';
import { getDatabase } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";

// Configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyBI6RyK4QIWVvGaVqdaVxlXJXr8GTX2xig",
    authDomain: "nursifyweb.firebaseapp.com",
    projectId: "nursifyweb",
    storageBucket: "nursifyweb.appspot.com",
    messagingSenderId: "450700024981",
    appId: "1:450700024981:web:ab5581390f43a8d7d50f9c"
};

// Inicializar Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getDatabase(app);

// Obtener referencia a los elementos del DOM
const btnRegistro = document.getElementById('btnRegistro');

// Función para realizar el registro
function registro() {
    const email = document.getElementById('email').value;
    const passwd = document.getElementById('contra').value;

    // Crear el usuario con correo electrónico y contraseña
    createUserWithEmailAndPassword(auth, email, passwd)
        .then((userCredential) => {
            // Registro exitoso
            alert("Se registró correctamente");
            window.location.href = "/index.html";
        })
        .catch((error) => {
            // Error en el registro
            alert("Hubo un error durante el registro: " + error.message);
        });
}

// Agregar evento click al botón de registro
btnRegistro.addEventListener('click', () => {
    registro();
});